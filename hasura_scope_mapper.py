ak_logger.info("hasura roles called")
hasura = {
  "x-hasura-default-role": "anonymous",
  "x-hasura-allowed-roles": []
}

if user:
  group_attrs = user.group_attributes()
  hasura["x-hasura-user-id"] = user.uid,
  hasura["x-hasura-allowed-roles"] = [
    "anonymous",
    *group_attrs.get("x-hasura-allowed-roles", []),
  ]

return {
  "https://hasura.io/jwt/claims": hasura,
}
