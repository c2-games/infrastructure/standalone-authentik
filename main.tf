terraform {
  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = ">=2024.2.0"
    }
  }
}

provider "authentik" {}

# GET default implicit auth flow - no prompt for user consent on authz
data "authentik_flow" "default-implicit-authorization-flow" {
  slug = "default-provider-authorization-implicit-consent"
}

data "authentik_scope_mapping" "profile" {
  scope_name = "profile"
}
data "authentik_scope_mapping" "email" {
  scope_name = "email"
}
data "authentik_scope_mapping" "offline_access" {
  # this scope is required for refresh tokens to be issued
  scope_name = "offline_access"
}

resource "authentik_group" "staff" {
  name = "Staff"
  attributes = jsonencode({"x-hasura-allowed-roles"=["staff"]})
}

resource "authentik_group" "participant" {
  name = "Participant"
  attributes = jsonencode({"x-hasura-allowed-roles"=["participant"]})
}

resource "authentik_group" "adversarial" {
  name = "Adversarial Team"
  parent = authentik_group.staff.id
  attributes = jsonencode({"x-hasura-allowed-roles"=["attacker"]})
}

resource "authentik_group" "ctf_admin" {
  name = "CTF Admin"
  parent = authentik_group.staff.id
  attributes = jsonencode({"x-hasura-allowed-roles"=["ctf_admin"]})
}

resource "authentik_group" "event_admin" {
  name = "Event Admin"
  parent = authentik_group.staff.id
  attributes = jsonencode({"x-hasura-allowed-roles"=["event_admin"]})
}

resource "authentik_scope_mapping" "hasura" {
  name       = "Hasura"
  scope_name = "hasura"
  expression = templatefile("hasura_scope_mapper.py", {})
}

# https://registry.terraform.io/providers/goauthentik/authentik/latest/docs/resources/provider_oauth2
# todo does a generic name make more sense here? "prod"? "oidc"?
resource "authentik_provider_oauth2" "scoreboard-ui" {
  name      = "Scoreboard UI"
  client_id = "scoreboard-ui"
  authorization_flow = data.authentik_flow.default-implicit-authorization-flow.id
  client_type = "public"
  issuer_mode = "global"
  redirect_uris = [
    "http://localhost:5173",
    "http://localhost:5174",
    "http://localhost:5175",
    ".*"
  ]
  property_mappings = [
    # authentik managed scopes
    data.authentik_scope_mapping.email.id,
    data.authentik_scope_mapping.profile.id,
    data.authentik_scope_mapping.offline_access.id,
    # custom scopes
    authentik_scope_mapping.hasura.id
  ]
}

resource "authentik_application" "scoreboard-ui" {
  name              = "Scoreboard UI"
  slug              = "scoreboard-ui"
  protocol_provider = authentik_provider_oauth2.scoreboard-ui.id
}
